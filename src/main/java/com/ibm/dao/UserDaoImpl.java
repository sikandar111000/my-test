package com.ibm.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ibm.entity.User;

@Transactional
@Repository("userDAO")
public class UserDaoImpl implements UserDAO
{
    @Autowired
    SessionFactory sessionFactory;
	
    
    
	@Override
	public boolean insert(User user)
	{
	  sessionFactory.getCurrentSession().save(user);
	  return true;
	}



	@Override
	public List<User> getUserDetails() 
	{
		return sessionFactory.getCurrentSession().createQuery("from User").list();
	}

}
