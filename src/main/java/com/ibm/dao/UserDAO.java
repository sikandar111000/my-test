package com.ibm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ibm.entity.User;

@Repository
public interface UserDAO 
{
  public boolean insert(User user); 
  public List<User> getUserDetails();
}
