package com.ibm.controller;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ibm.entity.User;
import com.ibm.services.UserService;

@Controller
//@RequestMapping("/")
public class UserController 
{
	@Autowired
	UserService userService;
	
	//upendra
	@RequestMapping(value = { "/", "/list" }, method = RequestMethod.GET)
    public String viewEmployeeDetails(ModelMap model) 
	{
        List<User> user1 = userService.userDetails();
        model.addAttribute("user1", user1);
        return "home";
    }
	
	
	@RequestMapping(value = { "/", "/adduser" }, method = RequestMethod.POST)
    public String addNewUser(ModelMap model,User user) throws ParseException
	{
       userService.saveData(user);
       System.out.println("Inserted Successfully");
        return "add";
    }
	
	

}
