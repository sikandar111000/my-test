package com.ibm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibm.dao.UserDAO;
import com.ibm.entity.User;

@Service("userService")
public class UserServiceImpl implements UserService 
{
	@Autowired
	UserDAO userDao;

	@Override
	public boolean saveData(User user) 
	{
		userDao.insert(user);
		return true;
	}

	@Override
	public List<User> userDetails() 
	{
		return userDao.getUserDetails();
	}

}
