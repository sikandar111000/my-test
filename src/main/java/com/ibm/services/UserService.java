package com.ibm.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibm.entity.User;

@Service
public interface UserService 
{
 public boolean saveData(User user); 
 public List<User> userDetails();
}
