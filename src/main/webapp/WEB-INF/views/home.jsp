<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Employee Details</title>
 
    <style>
        tr:first-child{
            font-weight: bold;
            background-color: #C6C9C4;
        }
    </style>
 
</head>
 
<body>
    <h2><center>Welcome to </center></h2>
    <h2>List of Employees</h2>  
    <table>
    <tr>
		<td>User Id</td>
		<td>NAME</td>
		<td>CardType</td>
	</tr>
        <c:forEach items="${user1}" var="employee">
            <tr>
            <td>${employee.uid}</td>
			<td>${employee.uname}</td>
            <td>${employee.cardType}</td>
            <%-- <td><a href="<c:url value='/edit-${employee.ssn}-employee' />">Edit</a></td>
			<td><a href="<c:url value='/delete-${employee.ssn}-employee' />">Delete</a></td> --%>
            </tr>
        </c:forEach>
    </table>
    <br/>
    <a href="<c:url value='/new' />">Add New Employee</a>
    <jsp:include page="add.jsp"/>
</body>
</html>

